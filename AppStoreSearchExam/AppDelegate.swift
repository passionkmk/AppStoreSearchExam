//
//  AppDelegate.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 11..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

/*
 *
 
 개발환경: Xcode 9.4.1, Swift 4.1
 사용한 라이브러리(Cocoapods):
 - RxSwift (https://github.com/ReactiveX/RxSwift)
 - Realm (https://github.com/realm/realm-cocoa)
 - SwiftyJSON (https://github.com/SwiftyJSON/SwiftyJSON)
 
 *
 */

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        // Reamlm 마이그레이션 세팅
        Realm.Configuration.defaultConfiguration = Realm.Configuration(schemaVersion: 0)
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        
    }

    func applicationWillTerminate(_ application: UIApplication) {
        
    }
}

