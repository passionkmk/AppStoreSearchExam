//
//  ImageOperation.swift
//  CustomImages
//
//  Created by KimMinKu on 2018. 4. 13..
//  Copyright © 2018년 MinKu Kim. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Init, Overrdies for Operation
class ImageDownLoader: Operation {
    let imageView: UIImageView
    let url: URL
    let fileName: String
    var defaultImage: UIImage
    
    init(imageView: UIImageView, url: URL, fileName: String, defaultImage: UIImage? = UIImage()) {
        self.imageView = imageView
        self.url = url
        self.fileName = fileName
        self.defaultImage = defaultImage!
    }
    
    // Opeation Task
    override func main() {
        self.imageView.alpha = 0.0
        URLSession.shared.dataTask(with: self.url) { (data, _, error) in
            if let error = error {
                d(error)
                self.loadFail()
            }
            else {
                guard let data = data else {
                    self.loadFail()
                    return
                }
                DispatchQueue.main.async {
                    self.loadSuccess(with: data)
                }
            }
        }.resume()
    }
}

// MARK: - Function
extension ImageDownLoader {
    private func loadSuccess(with data: Data) {
        // 이미지 캐시에 저장
        Tools.storeToCache(with: data, fileName: self.fileName)
        self.imageView.image = UIImage(data: data)
        UIView.animate(withDuration: 0.3, animations: {
            self.imageView.alpha = 1.0
        })
    }
    
    private func loadFail() {
        self.imageView.image = self.defaultImage
        UIView.animate(withDuration: 0.3, animations: {
            self.imageView.alpha = 1.0
        })
    }
}
