//
//  ImageOperationQueue.swift
//  CustomImages
//
//  Created by MinKu Kim on 2018. 4. 12..
//  Copyright © 2018년 MinKu Kim. All rights reserved.
//

import Foundation
import UIKit

// MARK: - Definition
protocol ImageOperationProtocol: class {
    // Operation 생성 및 제거하기위한 Hash 구조
    var downloadsOperations: [String: [String: Operation]] { get set }
    var operationQueue: OperationQueue { get }
}

// MARK: - Init
class ImageOperationQueue: ImageOperationProtocol {
    var downloadsOperations: [String : [String : Operation]] = [:]
    var operationQueue: OperationQueue {
        let queue = OperationQueue()
        // 최대 처리 Task 가능 개수
        queue.maxConcurrentOperationCount = 1
        queue.name = "DOWNLOAD_QUEUE"
        return queue
    }
    
    static var shared: ImageOperationQueue = ImageOperationQueue()
    
    public func setImage(with imageView: UIImageView, defaultImage: UIImage? = nil, url: String, indexPath: IndexPath, file: String = #file, specailKey: String? = nil) {
        guard let url = URL(string: url) else {
            return
        }
        
        // 캐시처리 위한 파일이름
        let fileName = "\(url)+ForKAKAOBANK".toMD5()
        // Operation Hash Key 값
        let fileNameKey = (file as NSString).lastPathComponent
        
        Tools.isCacheFileExist(fileName: fileName) ?
            self.loadFromCache(with: imageView, defaultImage: defaultImage, fileName: fileName) :
            self.storeToCacheQueue(with: imageView, defaultImage: defaultImage, url: url, indexPath: indexPath, fileName: fileName, fileNameKey: fileNameKey)
    }
    
    public func removeImage(indexPath: IndexPath, file: String = #file, specailKey: String? = nil) {
        let fileNameKey = (file as NSString).lastPathComponent
        var operationKey = "\(indexPath.section)+\(indexPath.row)"
        if let specail = specailKey {
            operationKey += specail
        }

        guard let operation = self.downloadsOperations[fileNameKey]?[operationKey] else {
            return
        }
        
        operation.cancel()
        self.downloadsOperations[fileNameKey]?.removeValue(forKey: operationKey)
    }
}

extension ImageOperationQueue {
    // 다운로드 필요한 이미지 Opearation 생성
    fileprivate func storeToCacheQueue(with imageView: UIImageView, defaultImage: UIImage? = nil, url: URL, indexPath: IndexPath, fileName: String,  fileNameKey: String, specailKey: String? = nil) {
        // IndexPath내 section, row 넘버사용하여 Hash key 설정
        var operationKey = "\(indexPath.section)+\(indexPath.row)"
        // Cell 내 ImageView가 여러개 있을시 구분짓기 위한 Key
        if let specail = specailKey {
            operationKey += specail
        }
        
        if (self.downloadsOperations[fileNameKey]?[operationKey]) != nil {
            return
        }
        
        let downLoader = ImageDownLoader(imageView: imageView, url: url, fileName: fileName, defaultImage: defaultImage)
        downLoader.completionBlock = {
            guard self.downloadsOperations[fileNameKey]?[operationKey] != nil else {
                return
            }
            
            self.downloadsOperations[fileNameKey]?.removeValue(forKey: operationKey)
        }
        
        self.downloadsOperations.updateValue([operationKey: downLoader], forKey: fileNameKey)
        self.operationQueue.addOperation(downLoader)
    }
    
    // 캐시성공 이미지 로드
    fileprivate func loadFromCache(with imageView: UIImageView, defaultImage: UIImage? = UIImage(), fileName: String) {
        imageView.alpha = 0.0
        let cachePath = Tools.getCacheFilePath(fileName: fileName)
        guard let cachedImage = UIImage(contentsOfFile: cachePath) else {
            imageView.image = defaultImage
            UIView.animate(withDuration: 0.3, animations: {
                imageView.alpha = 1.0
            })
            return
        }
        imageView.image = cachedImage
        UIView.animate(withDuration: 0.3, animations: {
            imageView.alpha = 1.0
        })
    }
}
