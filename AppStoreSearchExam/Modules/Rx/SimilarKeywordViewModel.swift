//
//  SimilarKeywordViewModel.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 14..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

enum SimilarKeywordResult {
    case start
    case showing(keywords: [SimilarKeywordCellViewModel])
    case validationFail(message: String)
    case fail(message: String)
}

extension SimilarKeywordResult {
    var similarKeywords: [SimilarKeywordCellViewModel] {
        switch self {
        case let .showing(keywords):
            return keywords
        default:
            return []
        }
    }
    
    var description: String? {
        switch self {
        case let .fail(message):
            return message
        case let .validationFail(messge):
            return messge
        default:
            return nil
        }
    }
    
    
}

class SimilarKeywordViewModel {
    let keywords: Driver<SimilarKeywordResult>
    
    init(searchText: Driver<String>) {
        self.keywords = searchText
            .distinctUntilChanged()
            .flatMapLatest({ (text) -> Driver<SimilarKeywordResult> in
                guard text.isRegex("^[가-힣\\s]+$") else {
                    return Driver.just(SimilarKeywordResult.validationFail(message: "한글 입력만 가능합니다."))
                }
                
                let searchKeywordModule = SearchKeywordModule()
                let similarKeys = searchKeywordModule.getSimilarKeywords(text)
                let similarKeyModels = similarKeys.map { SimilarKeywordCellViewModel(keyword: $0, similarKey: text) }
                return Observable
                    .just(.showing(keywords: similarKeyModels))
                    .startWith(.start)
                    .asDriver(onErrorJustReturn: .fail(message: "Rx Module Fail."))
            })
    }
}
