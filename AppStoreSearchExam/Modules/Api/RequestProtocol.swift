//
//  RequestProtocol.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 11..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import Foundation

enum MethodType: String {
    case post = "POST"
    case get = "GET"
}

// MARK: - Definition
protocol RequestProtocol: class {
    var request: NSMutableURLRequest { get }
}

// MARK: - Functions
extension RequestProtocol {
    func setRequest(url: String, method: MethodType, prameter: [String: String]? = nil, timeOut: TimeInterval? = nil) {
        var urlString = url.addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        if let param = prameter {
            urlString = self.combineToParameters(params: param, url: url).addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)
        }
        
        guard let resultUrl = urlString else {
            return
        }
        
        self.request.url = URL(string: resultUrl)
        self.request.httpMethod = method.rawValue
        self.request.timeoutInterval = timeOut == nil ? 10 : timeOut!
        
        // JSON 헤더 설정
        self.request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        self.request.addValue("application/json", forHTTPHeaderField: "Accept")
    }
    
    private func combineToParameters(params: [String: String], url: String) -> String {
        let queryKey = url.range(of: "?") != nil ? "&" : "?"
        return url + queryKey + self.converToString(parameters: params)
    }
    
    private func converToString(parameters: [String: String]) -> String {
        var paramString = ""
        parameters.forEach { (key, value) in
            paramString += "&\(key)=\(value)"
        }
        return String(paramString.dropFirst())
    }
}


