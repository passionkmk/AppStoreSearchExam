//
//  RequestModule.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 11..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import Foundation

typealias responseData = (Any?, Error?) -> Void

// MARK: - Init
public class RequestModule: RequestProtocol {
    static let shared: RequestModule = RequestModule()
    
    var request: NSMutableURLRequest = NSMutableURLRequest()
    
    func callSessesion(url: String, method: MethodType, prameter: [String: String]? = nil, timeOut: TimeInterval? = nil, completion: @escaping responseData) {
        self.setRequest(url: url, method: method, prameter: prameter, timeOut: timeOut)
        let session = URLSession(configuration: self.cofigration)
        let task = session.dataTask(with: self.request as URLRequest) { (data, response, error) in
            guard
                let httpResponse = response as? HTTPURLResponse,
                let resultData = data
                else {
                completion(nil, error)
                d("Error: response not extist.")
                return
            }
            
            switch httpResponse.statusCode {
            // 성공
            case 200:
                guard let json = self.dataToJSON(resultData) else {
                    completion(nil, error)
                    d("Error: json parse fail.")
                    return
                }
                completion(json, error)
                break
            // 실패
            default:
                completion(nil, error)
                d("Error: response not success.")
                break
            }
        }
        task.resume()
    }
}

// MARK: - Value
extension RequestModule {
    private var cofigration: URLSessionConfiguration {
        return .default
    }
}

// MARK: - Function
extension RequestModule {
    func dataToJSON(_ data: Data) -> Any? {
        do {
            return try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
        }
        catch let error {
            d(error)
            return nil
        }
    }
}
