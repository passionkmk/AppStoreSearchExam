//
//  SearchKeywordModule.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 13..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import Foundation
import RealmSwift

// MARK: - Public Functions
public class SearchKeywordModule {
    func updateKeyword(_ word: String) {
        let realm = try! Realm()
        if let exist = realm.objects(SearchKeyword.self).filter("keyword = '\(word)'").first {
            do {
                try realm.write {
                    exist.searchingDate = Date()
                }
            }
            catch {
                d("Search Keyword Update Fail.")
            }
        }
        else {
            let searchKeyword = SearchKeyword()
            searchKeyword.keyword = word
            searchKeyword.searchingDate = Date()
            do {
                try realm.write {
                    realm.add(searchKeyword)
                }
            }
            catch {
                d("Search Keyword Save Fail.")
            }
        }
    }
    
    func getRecentKeywords(_ count: Int) -> [String] {
        let realm = try! Realm()
        let datas = realm.objects(SearchKeyword.self).sorted(byKeyPath: "searchingDate", ascending: false)
        let keywords: [String] = datas.map { $0.keyword }
        if count == 0 {
            return keywords
        }
        else {
            return keywords.isEmpty ? keywords : Array(keywords[0 ..< min(keywords.count, count)])
        }
    }
    
    func getSimilarKeywords(_ word: String) -> [String] {
        let realm = try! Realm()
        let datas = realm.objects(SearchKeyword.self).filter("keyword contains[c] %@", word)
        return datas.map { $0.keyword }
    }
}

