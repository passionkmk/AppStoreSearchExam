//
//  RealmDataModel.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 13..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import Foundation
import RealmSwift

class SearchKeyword: Object {
    @objc dynamic var keyword: String = ""
    @objc dynamic var searchingDate: Date = Date()
}
