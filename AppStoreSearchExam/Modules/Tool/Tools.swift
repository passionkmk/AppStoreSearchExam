//
//  Tools.swift
//  CustomImages
//
//  Created by MinKu Kim on 2018. 4. 12..
//  Copyright © 2018년 MinKu Kim. All rights reserved.
//

import Foundation
import UIKit

class Tools {
    static func getCacheFilePath(fileName : String) -> String {
        let path: NSString = NSSearchPathForDirectoriesInDomains(.cachesDirectory, .userDomainMask, true)[0] as NSString
        return path.appendingPathComponent(fileName)
    }
    
    static func isCacheFileExist(fileName: String) -> Bool {
        let path = self.getCacheFilePath(fileName: fileName)
        return FileManager.default.fileExists(atPath: path)
    }
    
    static func storeToCache(with data: Data, fileName: String) {
        let path = Tools.getCacheFilePath(fileName: fileName)
        let data: NSData = NSData(data: data)
        data.write(toFile: path, atomically: true)
    }
    
    static func outBrowser(url: String) -> Bool {
        if let outUrl: URL = URL(string: url) {
            UIApplication.shared.open(outUrl, options: [:], completionHandler: nil)
            return true
        }
        else {
            return false
        }
    }
}
