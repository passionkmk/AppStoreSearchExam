//
//  CustomExtension.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 13..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit

// MAKR: - Properties
extension Array {
    func get(index: Int) -> Element? {
        return index >= 0 && index < self.count ? self[index] : nil
    }
}

extension String {
    public func isRegex(_ pattern: String) -> Bool {
        do {
            let regex = try NSRegularExpression(pattern: pattern, options: [])
            return regex.firstMatch(in: self, options: [], range: NSMakeRange(0, self.count)) != nil
        }
        catch {
            return false
        }
    }
    
    func toMD5() -> String {
        guard let messageData = self.data(using: .utf8) else {
            return "NO_DATA"
        }
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        
        return digestData.base64EncodedString()
    }
    
    func ISOtoDate() -> Date? {
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "ko_KR")
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
//        2015-04-02T01:04:53Z
        return dateFormatter.date(from: self)
    }
}

// MARK - UI
extension UIView {
    func setRound(_ radious: CGFloat? = 8.0) {
        self.layer.cornerRadius = self.frame.height / radious!
        self.layer.masksToBounds = true
    }
    
    func showView(_ duration: TimeInterval? = 0.3) {
        guard self.isHidden else {
            return
        }
        
        self.isHidden = false
        self.alpha = 0.0
        
        UIView.animate(withDuration: duration!) {
            self.alpha = 1.0
        }
    }
    
    func hideView(_ duration: TimeInterval? = 0.3) {
        guard !self.isHidden else {
            return
        }
        
        self.alpha = 1.0
        
        UIView.animate(withDuration: duration!, animations: {
            self.alpha = 0.0
        }) { (_) in
            self.isHidden = true
        }
    }
    
    func setHeightTransform(_ height: CGFloat) {
        self.transform = self.transform.scaledBy(x: 1, y: height)
    }
}

extension UILabel {
    func getHeight() -> CGFloat {
        guard
            let text: String = self.text,
            let font: UIFont = self.font
            else {
                return 0.0
        }
        
        let label: UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.font = font
        label.text = text
        
        label.sizeToFit()
        return label.frame.height
    }
}

extension UIImageView {
    // TODO: - 중복코드 개선 필요
    func loadImage(url: String, defaultImage: UIImage, completionHandler: ((_ image: UIImage?) -> Void)? = nil, errorHandler: ((_ error: Error? ) -> Void)? = nil) {
        guard let url = URL(string: url) else {
            self.image = defaultImage
            errorHandler?(nil)
            return
        }
        
        let fileName = "\(url)+ForKAKAOBANK".toMD5()
        
        // 캐시성공
        if Tools.isCacheFileExist(fileName: fileName) {
            self.alpha = 0.0
            let cachePath = Tools.getCacheFilePath(fileName: fileName)
            guard let cachedImage = UIImage(contentsOfFile: cachePath) else {
                self.image = defaultImage
                UIView.animate(withDuration: 0.3, animations: {
                    self.alpha = 1.0
                })
                completionHandler?(nil)
                return
            }
            self.image = cachedImage
            UIView.animate(withDuration: 0.3, animations: {
                self.alpha = 1.0
            })
            completionHandler?(cachedImage)
            return
        }
        
        // 이미지 다운로드 필요
        self.alpha = 0.0
        URLSession.shared.dataTask(with: url) { (data, _, error) in
            guard error == nil else {
                errorHandler?(error)
                return
            }
            
            guard let data = data, let image = UIImage(data: data) else {
                completionHandler?(nil)
                return
            }
            
            DispatchQueue.main.async {
                Tools.storeToCache(with: data, fileName: fileName)
                self.image = image
                UIView.animate(withDuration: 0.3, animations: {
                    self.alpha = 1.0
                })
                completionHandler?(image)
            }
        }.resume()
    }
}

enum LoadFailButtonType: String {
    case cancel = "취소"
    case reload = "재시도"
}
extension LoadFailButtonType {
    static var all: [LoadFailButtonType] {
        return [.cancel, .reload]
    }
}

extension UIViewController {
    func loadFail(buttonHandler: @escaping (_ type: LoadFailButtonType) -> Void) {
        let alertController = UIAlertController(title: "통신 장애", message: "데이터 로드에 실패 하였습니다. \n 재시도 하시겠습니까?", preferredStyle: .alert)
        LoadFailButtonType.all.forEach { (type) in
            let action = UIAlertAction(title: type.rawValue, style: type == .reload ? .default : .destructive, handler: { (action) in
                buttonHandler(type)
            })
            alertController.addAction(action)
        }
        self.present(alertController, animated: true, completion: nil)
    }
}


