//
//  MainViewController.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 13..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import SwiftyJSON

enum MainShowType {
    case clear
    case editing
    case done
}

// MARK: - Overrides
class MainViewController: UIViewController, Pageable {
    
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var searchStatusLabel: UILabel!
    @IBOutlet weak var searchButton: UIButton!
    
    @IBOutlet weak var searchTableView: UITableView!
    @IBOutlet weak var similarTableView: UITableView!
    
    @IBOutlet weak var noResultView: UIView!
    @IBOutlet weak var noResultKeywordLabel: UILabel!
    
    @IBOutlet weak var similarKeywordView: UIView!
    @IBOutlet weak var similarKeywordViewBottomConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var topSearchViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topTextFieldViewHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var searchTitleBottomConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchTextFieldTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchTextFieldCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var searchButtonWidthConstraint: NSLayoutConstraint!
    
    // MARK: - Paging Properties
    var currentPage: Int = 0
    var pagePerCount: Int { return 50 }
    var currentCount: Int { return self.searchDatas.count }
    var isShouldShowLoadingCell: Bool = false
    
    let searchKeywordModule = SearchKeywordModule()
    
    var similarKeywordModels: [SimilarKeywordCellViewModel] = []
    var searchDatas: [SearchData] = []
    var disposeBag: DisposeBag = DisposeBag()
    
    var topSearchViewHeightInitial: CGFloat = 0.0
    var searchTitileBottomInitial: CGFloat = 0.0
    var searchTextFieldTrailingInitial: CGFloat = 0.0
    var searchTextFieldCenterInitial: CGFloat = 0.0
    var searchButtonWidthInitial: CGFloat = 0.0
    
    var showType: MainShowType = .clear
    
    var isValid: Bool = true

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setKeyboardNotification()
        self.addGeusture()
        self.initContstant()
        self.initLoadingTableCell(tableView: self.searchTableView)
        self.binding()
        self.setClearUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()

    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        switch segue.identifier {
        case "AppDetailSegue":
            guard let appData = sender as? AppData, let detail = segue.destination as? AppDetailViewController else {
                return
            }
            detail.appData = appData
            break
        default:
            break
        }
    }
}

// MARK: - Extension Value
extension MainViewController {
    var searchKeyword: String? {
        guard let text = self.searchTextField.text, text.count > 0 else {
            return nil
        }
        return text
    }
    
    var recentKeywords: [String] {
        return self.searchKeywordModule.getRecentKeywords(10)
    }
}

// MARK: - Paging Function
extension MainViewController {
    func fetchNextPage() {
        self.currentPage += 1
        self.loadData()
    }
    
    func reset() {
        self.currentPage = 0
        self.searchDatas.removeAll()
        self.loadData()
    }
}

// MARK: - Actions
extension MainViewController {
    @IBAction func cacelSearch(_ sender: UIButton) {
        self.setClearUI()
    }
    
    @objc func similarKeywordViewTap(_ sender : UITapGestureRecognizer) {
        self.setClearUI()
    }
}

// MARK: - Functions
extension MainViewController {
    func initContstant() {
        self.topSearchViewHeightInitial = self.topSearchViewHeightConstraint.constant
        self.topTextFieldViewHeightConstraint.constant = self.topSearchViewHeightInitial / 2
        self.searchTitileBottomInitial = self.searchTitleBottomConstraint.constant
        self.searchTextFieldTrailingInitial = self.searchTextFieldTrailingConstraint.constant
        self.searchTextFieldCenterInitial = 10.0
        self.searchButtonWidthInitial = self.searchButtonWidthConstraint.constant
    }
    
    func binding() {
        // UI 객체는 Driver가 유용, Scheduler, Thread 지정을 안해주어 용이
        let similarViewModel = SimilarKeywordViewModel(searchText: self.searchTextField.rx.text.orEmpty.asDriver())
        similarViewModel.keywords.drive(onNext: { (result) in
            self.searchStatusLabel.text = nil
            self.isValid = true
            switch result {
            case .start:
                break
            case .fail:
                self.setClearUI()
                break
            case .validationFail:
                guard self.showType == .editing else {
                    return
                }
                self.isValid = false
                self.similarTableView.isHidden = true
                self.searchStatusLabel.text = result.description
                break
            case .showing:
                guard result.similarKeywords.count > 0 else {
                    self.similarTableView.isHidden = true
                    return
                }
                if self.showType != .editing {
                    self.setEditingUI()
                }
                self.similarTableView.isHidden = false
                self.similarKeywordModels = result.similarKeywords
                self.similarTableView.reloadData()
                break
            }
        }).disposed(by: self.disposeBag)
    }
    
    func loadData() {
        guard let keyword = self.searchKeyword else {
            return
        }
        
        let path = "https://itunes.apple.com/search"
        let param: [String: String] = ["term": keyword, "limit": String(self.pagePerCount), "country": "KR", "media": "software", "entity": "software", "offset": String(self.currentPage)]
        RequestModule.shared.callSessesion(url: path, method: .get, prameter: param) { (data, error) in
            guard error == nil else {
                self.loadFail(buttonHandler: { $0 == .reload ? self.loadData() : self.setClearUI() })
                return
            }
            let json = JSON(data)
            let results = json["results"].arrayValue
            
            if results.count == 0 && self.searchDatas.count == 0 {
                self.noResultView.showView()
                self.noResultKeywordLabel.text = "'\(keyword)'"
                self.setDoneUI()
                DispatchQueue.main.async {
                    self.searchTableView.reloadData()
                }
                return
            }
            
            if self.showType != .done {
                self.setDoneUI()
            }
            self.isShouldShowLoadingCell = results.count == self.pagePerCount
            self.searchDatas += results.map { SearchData(type: .search, search: SearchCellViewModel(content: AppData($0))) }
            DispatchQueue.main.async {
                self.searchTableView.reloadData()
            }
        }
    }
    
    func showingUIforType() {
        DispatchQueue.main.async {
            switch self.showType {
            case .clear:
                self.topSearchViewHeightConstraint.constant = self.topSearchViewHeightInitial
                self.searchTitleBottomConstraint.constant = self.searchTitileBottomInitial
                self.searchTextFieldCenterConstraint.constant = 0.0
                self.searchTextFieldTrailingConstraint.constant = 0.0
                self.searchButtonWidthConstraint.constant = 0.0
                break
            case .editing:
                self.topSearchViewHeightConstraint.constant = self.topSearchViewHeightInitial / 2
                self.searchTitleBottomConstraint.constant = self.searchTitileBottomInitial * 10
                self.searchTextFieldCenterConstraint.constant = self.searchTextFieldCenterInitial
                self.searchTextFieldTrailingConstraint.constant = self.searchTextFieldTrailingInitial
                self.searchButtonWidthConstraint.constant = self.searchButtonWidthInitial
                break
            case .done:
                self.topSearchViewHeightConstraint.constant = self.topSearchViewHeightInitial / 2
                self.searchTitleBottomConstraint.constant = self.searchTitileBottomInitial * 10
                self.searchTextFieldCenterConstraint.constant = 0.0
                self.searchTextFieldTrailingConstraint.constant = 0.0
                self.searchButtonWidthConstraint.constant = 0.0
                break
            }
            
            self.searchButton.alpha = self.showType == .editing ? 0.0 : 1.0
            UIView.animate(withDuration: 0.3) {
                self.searchButton.alpha = self.showType == .editing ? 1.0 : 0.0
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func setClearUI() {
        self.searchTextField.resignFirstResponder()
        self.searchStatusLabel.text = nil
        self.showType = .clear
        self.noResultView.hideView()
        self.showingUIforType()
        self.searchDatas.removeAll()
        self.searchDatas = self.recentKeywords.map { SearchData(type: .recent, recent: RecentKeywordCellViewModel(keyword: $0)) }
        self.searchDatas.insert(SearchData(type: .title), at: 0)
        self.searchTableView.reloadData()
    }
    
    func setEditingUI() {
        self.showType = .editing
        self.showingUIforType()
    }
    
    func setDoneUI() {
        self.showType = .done
        self.showingUIforType()
    }
}

// MARK: - Keyboard Notification
extension MainViewController {
    func setKeyboardNotification() {
        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.keyboardWillShow(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(MainViewController.keyboardWillHide(notification:)), name: .UIKeyboardWillHide, object: nil)
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            let keyboardHeight = keyboardSize.height
            self.setEditingUI()
            self.similarKeywordView.showView()
            self.similarKeywordViewBottomConstraint.constant = keyboardHeight - self.bottomLayoutGuide.length
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification){
        self.similarKeywordView.hideView()
        self.similarKeywordViewBottomConstraint.constant = 0.0
    }
}

// MARK: - Custom UIGeusture
extension MainViewController {
    func addGeusture() {
        let tapGesture = UITapGestureRecognizer(target: self, action:  #selector(MainViewController.similarKeywordViewTap(_:)))
        tapGesture.delegate = self
        self.similarKeywordView.addGestureRecognizer(tapGesture)
    }
}

// MARK: - UITableView Datasource
extension MainViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        switch self.showType {
        case .editing:
            return self.similarKeywordModels.count
        default:
            return self.isShouldShowLoadingCell && self.showType == .done ? self.searchDatas.count + 1 : self.searchDatas.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        switch self.showType {
        case .editing:
            guard tableView == self.similarTableView, let model = self.similarKeywordModels.get(index: indexPath.row) else {
                return UITableViewCell()
            }
            return model.loadTableCell(tableView, indexPath: indexPath)
        default:
            guard tableView == self.searchTableView, let data = self.searchDatas.get(index: indexPath.row) else {
                return self.isShouldShowLoadingCell ? self.getLodingTableCell(tableView: tableView) : UITableViewCell()
            }
            switch data.showType {
            case .title:
                return data.titile.loadTableCell(tableView, indexPath: indexPath)
            case .recent:
                return data.recent!.loadTableCell(tableView, indexPath: indexPath)
            case .search:
                return data.search!.loadTableCell(tableView, indexPath: indexPath)
            }
        }
    }
}

// MARK: - UITableView Delegate
extension MainViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch self.showType {
        case .clear:
            guard tableView == self.searchTableView, let data = self.searchDatas.get(index: indexPath.row) else {
                return
            }
            if data.showType == .recent {
                self.searchTextField.text = data.recent!.keyword
                self.reset()
            }
            break
        case .editing:
            guard tableView == self.similarTableView, let model = self.similarKeywordModels.get(index: indexPath.row) else {
                return
            }
            self.searchTextField.resignFirstResponder()
            self.searchTextField.text = model.keyword
            self.reset()
            break
        case .done:
            guard tableView == self.searchTableView, let data = self.searchDatas.get(index: indexPath.row) else {
                return
            }
            self.performSegue(withIdentifier: "AppDetailSegue", sender: data.search!.content)
            break
        }
    }
    
    func tableView(_ tableView: UITableView, didEndDisplaying cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        switch self.showType {
        case .done:
            guard tableView == self.searchTableView, let data = self.searchDatas.get(index: indexPath.row) else {
                return
            }
            if data.showType == .search {
                ImageOperationQueue.shared.removeImage(indexPath: indexPath, specailKey: data.search!.thumbnailKey)
                ImageOperationQueue.shared.removeImage(indexPath: indexPath, specailKey: data.search!.firstShotKey)
                ImageOperationQueue.shared.removeImage(indexPath: indexPath, specailKey: data.search!.secondsShotKey)
                ImageOperationQueue.shared.removeImage(indexPath: indexPath, specailKey: data.search!.thirdsShotKey)
            }
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        guard self.isLoadingIndexPath(indexPath), self.showType == .done else { return }
        self.fetchNextPage()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat  {
        switch self.showType {
        case .clear:
            return indexPath.row == 0 ? 52.0 : 44.0
        case .editing:
            return 44.0
        case .done:
            return 243.0
        }
    }
}

// MARK: - UITextView Delegate
extension MainViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        // TODO: - 플래그 방식 개선 필요함. 공개된 API가 아닌 방식이 있다고 하는데 위험성이 있어 배제함.
        guard self.isValid else {
            return false
        }
        
        guard let keyword = self.searchKeyword else {
            self.setClearUI()
            return false
        }
        
        self.searchTextField.resignFirstResponder()
        self.searchKeywordModule.updateKeyword(keyword)
        self.reset()
        return true
    }
}

// MARK: - UITapGesture Delegate
extension MainViewController: UIGestureRecognizerDelegate {
    // similarKeywordView Subview 인 tableView DidSelect 예외처리
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        guard let touchView = touch.view else {
            return false
        }
        if touchView.isDescendant(of: self.similarTableView) {
            return false
        }
        return true
    }
}


