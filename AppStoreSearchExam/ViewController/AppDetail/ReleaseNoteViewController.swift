//
//  ReleaseNoteViewController.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 15..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit

// MARK: - Overrides
class ReleaseNoteViewController: UIViewController {
    
    @IBOutlet weak var versionLabel: UILabel!
    @IBOutlet weak var releaseDateLabel: UILabel!
    
    @IBOutlet weak var noteLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    
    @IBOutlet weak var topInfoViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var moreButtonHeightConstraint: NSLayoutConstraint!
    
    var parentView: AppDetailViewController!
    
    let bottomSpacingInitial: CGFloat = 16.0
    var noteLabelHeightInitial: CGFloat = 0.0
    var topInfoViewHeightInitial: CGFloat = 0.0
    var moreButtonHeightInitial: CGFloat = 0.0

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: - Action
extension ReleaseNoteViewController {
    @IBAction func moreShow(_ sender: UIButton) {
        self.hideMoreUI()
        let viewHeight = self.topInfoViewHeightInitial + self.noteLabel.getHeight() + self.bottomSpacingInitial
        self.parentView.setReleaseNoteHeight(height: viewHeight)
    }
}

// MARK: - Fuction
extension ReleaseNoteViewController {
    func initUI() {
        self.noteLabel.text = "Loading..."
        self.noteLabelHeightInitial = self.noteLabel.getHeight()
        self.topInfoViewHeightInitial = self.topInfoViewHeightConstraint.constant
        self.moreButtonHeightInitial = self.moreButtonHeightConstraint.constant
    }
    
    func setUI(with appData: AppData) {
        self.versionLabel.text = "버전 \(appData.currentVersion)"
        self.releaseDateLabel.text = appData.lastUpdateAgo
        self.noteLabel.text = appData.releaseNote
        
        let noteLabelHeight: CGFloat = self.noteLabel.getHeight()
        
        var viewHeight: CGFloat = 0.0
        if noteLabelHeight > self.noteLabelHeightInitial * 3 {
            self.showMoreUI()
            viewHeight = self.topInfoViewHeightInitial + self.noteLabelHeightInitial * 3 + self.moreButtonHeightInitial + self.bottomSpacingInitial
        }
        else {
            self.hideMoreUI()
            viewHeight = self.topInfoViewHeightInitial + noteLabelHeight + self.bottomSpacingInitial
        }
        
        self.parentView.setReleaseNoteHeight(height: viewHeight)
    }
    
    func showMoreUI() {
        self.moreButton.isHidden = false
        self.moreButton.alpha = 0.0
        self.moreButtonHeightConstraint.constant = self.moreButtonHeightInitial
        UIView.animate(withDuration: 0.3) {
            self.moreButton.alpha = 1.0
            self.view.layoutIfNeeded()
        }
    }
    
    func hideMoreUI() {
        self.moreButton.alpha = 1.0
        self.moreButtonHeightConstraint.constant = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.moreButton.alpha = 0.0
            self.view.layoutIfNeeded()
        }) { (isFinish) in
            self.moreButton.isHidden = true
        }
    }
}
