//
//  AppDetailViewController.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 15..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit
import SwiftyJSON

// MARK: - Overrides
class AppDetailViewController: UIViewController {
    
    @IBOutlet weak var loadingView: UIView!
    
    @IBOutlet weak var topThumbnailImageView: UIImageView!
    
    @IBOutlet weak var appThumbnailimageView: UIImageView!
    @IBOutlet weak var appNameLabel: UILabel!
    @IBOutlet weak var developerNameLabel: UILabel!
    
    @IBOutlet weak var openButton: UIButton!
    @IBOutlet weak var sharedButton: UIButton!
    
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var ratingProgressView: UIProgressView!
    @IBOutlet weak var ratingCountLabel: UILabel!
    
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var advisoryLabel: UILabel!
    
    @IBOutlet weak var topThumbnailCenterYConstraint: NSLayoutConstraint!
    @IBOutlet weak var releaseNoteViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var screenShotViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var descriptionViewViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var infoViewHeightConstraint: NSLayoutConstraint!
    
    weak var releaseNoteViewController: ReleaseNoteViewController?
    weak var screenShotViewController: ScreenShotViewController?
    weak var descriptionViewController: DescriptionViewController?
    weak var infoViewController: InfoViewController?
    
    var appData: AppData!
    
    var topCenterInitial: CGFloat = 0.0
    
    let defaultImage: UIImage = UIImage(named: "DefaultThumbnail")!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()
        self.setUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // ContainerView 정의
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ReleaseNoteSegue" {
            guard let releaseView = segue.destination as? ReleaseNoteViewController else {
                return
            }
            self.releaseNoteViewController = releaseView
            releaseView.parentView = self
        }
        
        if segue.identifier == "ScreenShotSegue" {
            guard let screenShotView = segue.destination as? ScreenShotViewController else {
                return
            }
            self.screenShotViewController = screenShotView
            screenShotView.parentView = self
        }
        
        if segue.identifier == "DescriptionSegue" {
            guard let descriptionView = segue.destination as? DescriptionViewController else {
                return
            }
            self.descriptionViewController = descriptionView
            descriptionView.parentView = self
        }
        
        if segue.identifier == "InfoSegue" {
            guard let infoView = segue.destination as? InfoViewController else {
                return
            }
            self.infoViewController = infoView
            infoView.parentView = self
        }
    }
}

// MARK: - Actions
extension AppDetailViewController {
    @IBAction func back(_ sender: UIButton?) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func appOpen(_ sender: UIButton) {
        Tools.outBrowser(url: appData.shareUrl)
    }
    
    @IBAction func shared(_ sender: UIButton) {
        let actionSheet = UIAlertController(title: nil, message: nil, preferredStyle: UIAlertControllerStyle.actionSheet)
        let shared = UIAlertAction(title: "공유", style: UIAlertActionStyle.default, handler: {
            (action: UIAlertAction!) in
            self.shareApp()
        })
        
        let cancel = UIAlertAction(title: "취소", style: UIAlertActionStyle.cancel, handler: {
            (action: UIAlertAction!) in
        })
        
        actionSheet.addAction(shared)
        actionSheet.addAction(cancel)
        self.present(actionSheet, animated: true, completion: nil)
    }
}

// MARK: - Functions
extension AppDetailViewController {
    func initUI() {
        self.topThumbnailImageView.setRound()
        self.appThumbnailimageView.setRound()
        self.openButton.setRound(2)
        self.sharedButton.setRound(2)
        self.ratingProgressView.setHeightTransform(4)
        self.topCenterInitial = self.topThumbnailCenterYConstraint.constant
    }
    
    func setUI() {
        DispatchQueue.main.async {
            self.loadingView.hideView()
            self.setTopInfoView()
            self.setReleaseNoteView()
            self.setScreenShotView()
            self.setDescriptionView()
            self.setInfoView()
        }
    }
    
    func setTopInfoView () {
        self.topThumbnailImageView.loadImage(url: appData.smallThumbnailUrl, defaultImage: self.defaultImage)
        self.appThumbnailimageView.loadImage(url: appData.smallThumbnailUrl, defaultImage: self.defaultImage)
        self.appNameLabel.text = appData.name
        self.developerNameLabel.text = appData.develeoperName
        
        self.ratingLabel.text = String(format: "%.01f", appData.userRating)
        let progress = Float(appData.userRating) / 5.0
        self.ratingProgressView.setProgress(progress, animated: false)
        let divideK = appData.userRatingCount / 1000
        self.ratingCountLabel.text = divideK > 0 ? "\(divideK)천개" : ("\(appData.userRatingCount)개")
        
        self.genreLabel.text = appData.genre
        self.advisoryLabel.text = appData.advisory
    }
    
    func setReleaseNoteView() {
        guard let releaseView = self.releaseNoteViewController else {
            return
        }
        releaseView.setUI(with: appData)
    }
    
    func setScreenShotView() {
        guard let screenShotView = self.screenShotViewController else {
            return
        }
        screenShotView.setUI(with: appData)
    }
    
    func setDescriptionView() {
        guard let descriptionView = self.descriptionViewController else {
            return
        }
        descriptionView.setUI(with: appData)
    }
    
    func setInfoView() {
        guard let infoView = self.infoViewController else {
            return
        }
        infoView.setUI(appData: appData)
    }
    
    func popTopThumbnail() {
        guard self.topThumbnailImageView.alpha != 1.0 else {
            return
        }
        self.topThumbnailImageView.alpha = 0.0
        self.topThumbnailCenterYConstraint.constant = 0
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [.curveEaseInOut], animations: {
            self.topThumbnailImageView.alpha = 1.0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func hideTopThumbnail() {
        guard self.topThumbnailImageView.alpha != 0.0 else {
            return
        }
        self.topThumbnailImageView.alpha = 1.0
        self.topThumbnailCenterYConstraint.constant = self.topCenterInitial
        UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.5, initialSpringVelocity: 0, options: [.curveEaseInOut], animations: {
            self.topThumbnailImageView.alpha = 0.0
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func shareApp() {
        let appName: String = "[\(self.appData.name)]"
        var sharedUrl: String = self.appData.shareUrl
        
        let activityViewController: UIActivityViewController = UIActivityViewController(activityItems: ["\(appName)\n\(sharedUrl)"] , applicationActivities: nil)
        self.present(activityViewController, animated: true, completion: nil)
    }
}

// MARK: - For Child Function
extension AppDetailViewController {
    func setReleaseNoteHeight(height: CGFloat) {
        DispatchQueue.main.async {
            self.releaseNoteViewHeightConstraint.constant = height
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func setScreenShotHeight(height: CGFloat) {
        DispatchQueue.main.async {
            self.screenShotViewHeightConstraint.constant = height
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func setDescriptionHeight(height: CGFloat) {
        DispatchQueue.main.async {
            self.descriptionViewViewHeightConstraint.constant = height
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    func setInfoHeight(height: CGFloat) {
        DispatchQueue.main.async {
            self.infoViewHeightConstraint.constant = height
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
}

// MARK: - UIScrollView Delelgate
extension AppDetailViewController: UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        self.topThumbnailImageView.isHidden = false
        if scrollView.contentOffset.y > 50 {
            self.popTopThumbnail()
        }
        else {
            self.hideTopThumbnail()
        }
    }
}
