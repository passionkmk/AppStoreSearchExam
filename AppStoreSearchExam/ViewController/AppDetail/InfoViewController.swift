//
//  InfoViewController.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 15..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit

// MARK: - Overrides
class InfoViewController: UIViewController {
    
    @IBOutlet weak var infoTableView: UITableView!
    
    var parentView: AppDetailViewController!
    var infoModels: [InfoCellViewModel] = []
    
    let titles: [String] = ["정보", "판매자", "크기", "카테고리", "호환성", "언어", "연령", "개발자 웹사이트"]
    
    let headTitleCellHeightInitial: CGFloat = 52.0
    let infoCellHeightInitial: CGFloat = 44.0

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: - Function
extension InfoViewController {
    func setUI(appData: AppData) {
        self.infoModels = titles.map({ (title) -> InfoCellViewModel in
            switch title {
            case "정보":
                return InfoCellViewModel(title: title, info: "", type: .headTitle)
            case "판매자":
                return InfoCellViewModel(title: title, info: appData.sellerName, type: .info)
            case "크기":
                return InfoCellViewModel(title: title, info: appData.sizeString, type: .info)
            case "카테고리":
                return InfoCellViewModel(title: title, info: appData.genre, type: .info)
            case "호환성":
                return InfoCellViewModel(title: title, info: appData.minimuOSversion, type: .info)
            case "언어":
                guard let lang = appData.languages.get(index: 0) else {
                    return InfoCellViewModel(title: title, info: "", type: .info)
                }
                return InfoCellViewModel(title: title, info: lang, type: .info)
            case "연령":
                return InfoCellViewModel(title: title, info: appData.advisory, type: .info)
            case "개발자 웹사이트":
                return InfoCellViewModel(title: title, info: appData.sellerUrl, type: .link)
            default:
                return InfoCellViewModel(title: "", info: "", type: .info)
            }
        })
        
        self.infoTableView.reloadData()
        let viewHeight = self.headTitleCellHeightInitial + (self.infoCellHeightInitial * CGFloat(self.infoModels.count - 1))
        self.parentView.setInfoHeight(height: viewHeight)
    }
}

// MARK: - UITableView DataSource
extension InfoViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.infoModels.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let model = self.infoModels.get(index: indexPath.row) else {
            return UITableViewCell()
        }
        return model.loadTableCell(tableView, indexPath: indexPath)
    }
}

// MARK: - UITableView Delegate
extension InfoViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let model = self.infoModels.get(index: indexPath.row) else {
            return
        }
        switch model.type {
        case .link:
            Tools.outBrowser(url: model.info)
            break
        default:
            break
        }
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return indexPath.row == 0 ? self.headTitleCellHeightInitial : self.infoCellHeightInitial
    }
}
