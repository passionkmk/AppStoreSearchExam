//
//  ScreenShotViewController.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 15..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit

// MARK: - Overrides
class ScreenShotViewController: UIViewController {
    
    @IBOutlet weak var screenShotCollectionView: UICollectionView!
//    @IBOutlet weak var screenShotCollectionViewLeadingConstraint: NSLayoutConstraint!
    
    var parentView: AppDetailViewController!
    var screenshotModels: [ScreenShotCellViewModel] = []
    
    var imageSize: CGSize = CGSize(width: 240.0, height: 426.0)
    let imageWidthRatio: CGFloat = 0.95

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

// MARK: - Functions
extension ScreenShotViewController {
    func setUI(with appData: AppData) {
        self.screenshotModels = appData.screenShotUrls.map { ScreenShotCellViewModel(url: $0) }
        
        var viewHeight: CGFloat = 0.0
        if self.screenshotModels.count > 0 {
            if let size = appData.screenShotSize {
                self.imageSize = self.getImageSize(imageSize: size)
            }
            
            viewHeight = self.imageSize.height
            self.screenShotCollectionView.reloadData()
        }
        
        self.parentView.setScreenShotHeight(height: viewHeight)
    }
    
    func getImageSize(imageSize: CGSize) -> CGSize {
        let width = self.screenShotCollectionView.frame.size.width * 0.75 * self.imageWidthRatio
        let ratio = width / imageSize.width
        let height = ratio * imageSize.height
        return CGSize(width: width, height: height)
    }
}

// MARK: - UICollectionView DataSource
extension ScreenShotViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.screenshotModels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let model = self.screenshotModels.get(index: indexPath.row) else {
            return UICollectionViewCell()
        }
        return model.loadCollectionCell(collectionView, indexPath: indexPath)
    }
}

// MARK: - UICollectionView Delegate
extension ScreenShotViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        ImageOperationQueue.shared.removeImage(indexPath: indexPath)
    }
}

// MARK: - UICollectionView DelegateFlowLayout
extension ScreenShotViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return self.imageSize
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 8.0
    }
}
