//
//  DescriptionViewController.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 15..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit

// MARK: - Overrides
class DescriptionViewController: UIViewController {
    
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var moreButton: UIButton!
    
    @IBOutlet weak var moreButtonHeightConstraint: NSLayoutConstraint!
    
    let topSpacingInitial: CGFloat = 32.0
    let bottomSpacingInitial: CGFloat = 16.0
    var descriptLabelHeightInitial: CGFloat = 0.0
    var moreButtonHeightInitial: CGFloat = 0.0
    
    var parentView: AppDetailViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
}

//MARK: - Actions
extension DescriptionViewController {
    @IBAction func moreShow(_ sender: UIButton) {
        self.hideMoreUI()
        let viewHeight = self.topSpacingInitial + self.descriptionLabel.getHeight() + self.bottomSpacingInitial
        self.parentView.setDescriptionHeight(height: viewHeight)
    }
}

//MARK: - Fuction
extension DescriptionViewController {
    func initUI() {
        self.descriptionLabel.text = "Loading..."
        self.descriptLabelHeightInitial = self.descriptionLabel.getHeight()
        self.moreButtonHeightInitial = self.moreButtonHeightConstraint.constant
    }
    
    func setUI(with appData: AppData) {
        self.descriptionLabel.text = appData.description
        
        let descriptionLableHeight = self.descriptionLabel.getHeight()
        
        var viewHeight: CGFloat = 0.0
        if descriptionLableHeight > self.descriptLabelHeightInitial * 3 {
            self.showMoreUI()
            viewHeight = self.topSpacingInitial + self.descriptLabelHeightInitial * 3 + self.moreButtonHeightInitial + self.bottomSpacingInitial
        }
        else {
            self.hideMoreUI()
            viewHeight = self.topSpacingInitial + descriptionLableHeight + self.bottomSpacingInitial
        }
        
        self.parentView.setDescriptionHeight(height: viewHeight)
        
    }
    
    func showMoreUI() {
        self.moreButton.isHidden = false
        self.moreButton.alpha = 0.0
        self.moreButtonHeightConstraint.constant = self.moreButtonHeightInitial
        UIView.animate(withDuration: 0.3) {
            self.moreButton.alpha = 1.0
            self.view.layoutIfNeeded()
        }
    }
    
    func hideMoreUI() {
        self.moreButton.alpha = 1.0
        self.moreButtonHeightConstraint.constant = 0
        UIView.animate(withDuration: 0.3, animations: {
            self.moreButton.alpha = 0.0
            self.view.layoutIfNeeded()
        }) { (isFinish) in
            self.moreButton.isHidden = true
        }
    }
}
