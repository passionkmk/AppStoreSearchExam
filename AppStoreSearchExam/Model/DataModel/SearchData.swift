//
//  SearchData.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 15..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import Foundation

enum SearchShowType {
    case title
    case recent
    case search
}

// MARK: - Init
struct SearchData {
    var showType: SearchShowType
    let titile: TitleCellViewModel = TitleCellViewModel(title: "최근 검색어")
    var recent: RecentKeywordCellViewModel?
    var search: SearchCellViewModel?
    
    init(type: SearchShowType, recent: RecentKeywordCellViewModel? = nil, search: SearchCellViewModel? = nil) {
        self.showType = type
        self.recent = recent
        self.search = search
    }
}
