//
//  AppData.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 14..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import Foundation
import SwiftyJSON

// MARK: - Init
struct AppData {
    let id: String //trackId
    let name: String //trackName
    let develeoperName: String //artistName
    let sellerName: String //sellerName
    let smallThumbnailUrl: String //artworkUrl100
    let largeThumbnailUrl: String //artworkUrl512
    let shareUrl: String //trackViewUrl
    let sellerUrl: String //sellerUrl
    let genre: String //primaryGenreName
    let advisory: String //contentAdvisoryRating
    let languages: [String] //languageCodesISO2A
    let minimuOSversion: String //minimumOsVersion
    let currentVersion: String //version
    let currentVersionDate: Date? //currentVersionReleaseDate
    let description: String //description
    let releaseNote: String? //releaseNotes
    let screenShotUrls: [String] //screenshotUrls
    let fileSizeByte: Double //fileSizeBytes
    let userRating: Double //averageUserRating
    let userRatingCount: Int //userRatingCount
    
    init(_ data: JSON) {
        self.id = data["trackId"].stringValue
        self.name = data["trackName"].stringValue
        self.develeoperName = data["artistName"].stringValue
        self.sellerName = data["sellerName"].stringValue
        
        self.smallThumbnailUrl = data["artworkUrl100"].stringValue
        self.largeThumbnailUrl = data["artworkUrl512"].stringValue
        self.shareUrl = data["trackViewUrl"].stringValue
        self.sellerUrl = data["sellerUrl"].stringValue
        
        self.genre = data["primaryGenreName"].stringValue
        self.advisory = data["contentAdvisoryRating"].stringValue
        self.languages = data["languageCodesISO2A"].arrayValue.map { $0.stringValue }
        
        self.minimuOSversion = data["minimumOsVersion"].stringValue
        self.currentVersion = data["version"].stringValue
        
        let currentVersionDateString = data["currentVersionReleaseDate"].stringValue
        self.currentVersionDate = currentVersionDateString.ISOtoDate()
        
        self.description = data["description"].stringValue
        
        let rel = data["releaseNotes"].stringValue
        self.releaseNote = rel.count > 0 ? rel : nil
        
        let screenUrls = data["screenshotUrls"].arrayValue
        self.screenShotUrls = screenUrls.count > 0 ? screenUrls.map{ $0.stringValue } : []
        
        self.fileSizeByte = data["fileSizeBytes"].doubleValue
        self.userRating = data["averageUserRating"].doubleValue
        self.userRatingCount = data["userRatingCount"].intValue
    }
}

extension AppData {
    // TODO: - 파싱로직 개선 필요.
    var screenShotSize: CGSize? {
        guard !self.screenShotUrls.isEmpty else {
            return nil
        }
        
        let url: String = self.screenShotUrls[0]
        let parsedSlash: [String] = url.components(separatedBy: "/")
        let sizeIncludeString = parsedSlash.filter { $0.contains("bb.") }.first
        
        guard let sizeInfoString = sizeIncludeString else {
            return nil
        }
        let parsedX: [String] = sizeInfoString.components(separatedBy: "x")
        
        guard
            parsedX.count == 2,
            let heightString = parsedX[1].components(separatedBy: "bb").first else {
            return nil
        }
        let width = (parsedX[0] as NSString).floatValue
        let height = (heightString as NSString).floatValue
        return CGSize(width: CGFloat(width), height: CGFloat(height))
    }
    
    var lastUpdateAgo: String? {
        guard let updateDate = self.currentVersionDate else {
            return nil
        }
        
        let calendar = Calendar.current
        let update = calendar.startOfDay(for: updateDate)
        let today = calendar.startOfDay(for: Date())
        let components = calendar.dateComponents([.day], from: update, to: today)
        
        guard let days = components.day else {
            return nil
        }
        return days > 6 ? "\(days / 7)주전" : "\(days)일전"
    }
    
    var sizeString: String {
        let mb = self.fileSizeByte / pow(1024.0, 2.0)
        let kb = self.fileSizeByte / 1024.0
        return mb > 0 ? String(format: "%.01f", mb) + "MB" : String(format: "%.01f", kb) + "KB"
    }
    
    var mainLanguage: String? {
        guard let lang = self.languages.get(index: 0) else {
            return nil
        }
        return lang
    }
}

