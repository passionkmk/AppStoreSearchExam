//
//  ScreenShotCollectionViewCell.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 15..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit

// MARK: - Outlet
class ScreenShotCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var screenShotImageView: UIImageView!
    
    override func awakeFromNib() {
        self.screenShotImageView.setRound()
    }
}

// MARK: - Function
extension ScreenShotCollectionViewCell {
    func setUI(model: ScreenShotCellViewModel, indexPath: IndexPath) {
        ImageOperationQueue.shared.setImage(with: self.screenShotImageView,
                                            defaultImage: model.cellScreentShotDefaultImage,
                                            url: model.url,
                                            indexPath: indexPath,
                                            specailKey: nil)
    }
}


