//
//  ScreenShotCellViewModel.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 15..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit

// MARK: - Init
class ScreenShotCellViewModel: CellPresentation {
    var url: String
    init(url: String) {
        self.url = url
    }
}

extension ScreenShotCellViewModel {
    func loadCollectionCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ScreentShotCell", for: indexPath) as! ScreenShotCollectionViewCell
        cell.setUI(model: self, indexPath: indexPath)
        return cell
    }
}


