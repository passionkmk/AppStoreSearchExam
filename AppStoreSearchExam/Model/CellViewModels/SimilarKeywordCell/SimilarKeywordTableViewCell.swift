//
//  SimilarKeywordTableViewCell.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 14..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit

class SimilarKeywordTableViewCell: UITableViewCell {
    @IBOutlet weak var keywordLabel: UILabel!
    let similarWordColor = UIColor(red: 0.6627, green: 0.6627, blue: 0.6627, alpha: 1.0)
}

extension SimilarKeywordTableViewCell {
    func setLabel(_ keyword: String, similarKey: String) {
        self.keywordLabel.textColor = self.similarWordColor
        let string = keyword as NSString
        let attributeString = NSMutableAttributedString(string: keyword)
        let atttibute = [NSAttributedStringKey.foregroundColor: UIColor.black]
        attributeString.addAttributes(atttibute, range: string.range(of: similarKey))
        self.keywordLabel.attributedText = attributeString
    }
}
