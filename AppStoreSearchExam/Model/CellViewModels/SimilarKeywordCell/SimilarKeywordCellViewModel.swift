//
//  SimilarKeywordCellViewModel.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 14..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit

class SimilarKeywordCellViewModel: CellPresentation {
    var keyword: String
    var similarKeyword: String
    init(keyword: String, similarKey: String) {
        self.keyword = keyword
        self.similarKeyword = similarKey
    }
}

extension SimilarKeywordCellViewModel {
    func loadTableCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SimilarKeywordCell", for: indexPath) as! SimilarKeywordTableViewCell
        cell.setLabel(self.keyword, similarKey: self.similarKeyword)
        return cell
    }
}
