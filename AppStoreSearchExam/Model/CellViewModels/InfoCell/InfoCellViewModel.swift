//
//  InfoCellViewModel.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 15..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit

enum InfoCellType {
    case headTitle
    case info
    case link
}

// MARK: - Init
class InfoCellViewModel: CellPresentation {
    var title: String
    var info: String
    var type: InfoCellType
    init(title: String, info: String, type: InfoCellType) {
        self.title = title
        self.info = info
        self.type = type
    }
}

extension InfoCellViewModel {
    func loadTableCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "InfoCell", for: indexPath) as! InfoTableViewCell
        cell.setUI(model: self)
        return cell
    }
}
