//
//  InfoTableViewCell.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 15..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit

// MARK: - Outlet
class InfoTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var infoLabel: UILabel!
    
    let blueColor = UIColor(red: 0, green: 0.4784, blue: 1, alpha: 1.0)
}

extension InfoTableViewCell {
    func setUI(model: InfoCellViewModel)  {
        switch model.type {
        case .headTitle:
            self.titleLabel.text = model.title
            self.titleLabel.font = UIFont(name: "AppleSDGothicNeo-Bold", size: 17)
            self.titleLabel.textColor = .black
            break
        case .link:
            self.titleLabel.textColor = self.blueColor
            self.titleLabel.text = model.title
            break
        case .info:
            self.titleLabel.text = model.title
            self.infoLabel.text = model.info
            break
        }
    }
}


