//
//  TitleTableViewCell.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 13..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit

// MARK: - Outlet
class TitleTableViewCell: UITableViewCell {
    @IBOutlet weak var titleLabel: UILabel!
}

// MARK: - Functions
extension TitleTableViewCell {
    func setLabel(_ title: String) {
        self.titleLabel.text = title
    }
}
