//
//  TitleCellViewModel.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 13..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit

// MARK: - Init
class TitleCellViewModel: CellPresentation {
    var title: String = ""
    init(title: String) {
        self.title = title
    }
}

// MARK: - Essential Function
extension TitleCellViewModel {
    func loadTableCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "TitleCell", for: indexPath) as! TitleTableViewCell
        cell.setLabel(self.title)
        return cell
    }
}

