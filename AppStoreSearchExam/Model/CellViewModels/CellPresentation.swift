//
//  CellPresentation.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 13..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit

// MARK: - Definition
@objc protocol CellPresentation: class {
    @objc optional func loadTableCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell
    @objc optional func loadCollectionCell(_ collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell
}

// MARK: - Extension Object
extension CellPresentation {
    var cellThumbnailDefaultImage: UIImage {
        return UIImage(named: "DefaultThumbnail")!
    }
    
    var cellScreentShotDefaultImage: UIImage {
        return UIImage(named: "ScreenShotDeafaultImage")!
    }
}
