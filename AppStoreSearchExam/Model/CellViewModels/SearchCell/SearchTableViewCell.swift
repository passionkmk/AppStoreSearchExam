//
//  SearchTableViewCell.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 14..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit

// MARK: - Overrides
class SearchTableViewCell: UITableViewCell {

    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var appNameLabel: UILabel!
    
    // TODO: - 부제가 없어 앱설명으로 대체
    @IBOutlet weak var descriptionLabel: UILabel!
    
    // TODO: - 별모양 이미지가 없어 ProgressView로 대체
    @IBOutlet weak var ratingProgressView: UIProgressView!
    
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var ratingCountLabel: UILabel!
    @IBOutlet weak var showButton: UIButton!
    
    @IBOutlet weak var screenShotWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var screenShotCenterXConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var screenShotImageViewFirst: UIImageView!
    @IBOutlet weak var screenShotImageViewSeconds: UIImageView!
    @IBOutlet weak var screenShotImageViewThirds: UIImageView!
    
    var screenShotImageViewHeightInital: CGFloat = 0.0
    
    override func awakeFromNib() {
        self.thumbnailImageView.setRound()
        self.screenShotImageViewFirst.setRound(12)
        self.screenShotImageViewSeconds.setRound(12)
        self.screenShotImageViewThirds.setRound(12)
        self.screenShotImageViewHeightInital = self.screenShotImageViewSeconds.frame.size.height
        self.ratingProgressView.setHeightTransform(4)
    }
    
    override func prepareForReuse() {
        
    }
}

// MARK: - Function
extension SearchTableViewCell {
    func setUI(_ model: SearchCellViewModel, indexPath: IndexPath) {
        self.ratingProgressView.showView()
        self.showButton.showView()
        let appData = model.content
        ImageOperationQueue.shared.setImage(with: self.thumbnailImageView,
                                            defaultImage: model.cellThumbnailDefaultImage,
                                            url: appData.smallThumbnailUrl,
                                            indexPath: indexPath,
                                            specailKey: model.thumbnailKey)
        self.appNameLabel.text = appData.name
        self.descriptionLabel.text = appData.description
        
        let progress = Float(appData.userRating) / 5.0
        self.ratingProgressView.setProgress(progress, animated: false)
        
        self.ratingLabel.text = String(format: "%.01f", appData.userRating)
        
        let divideK = appData.userRatingCount / 1000
        self.ratingCountLabel.text = divideK > 0 ? "\(divideK)천개" : ("\(appData.userRatingCount)개")
        
        if let shotSize = appData.screenShotSize {
            let ratio = self.screenShotImageViewHeightInital / shotSize.height
            let shotWidth = ratio * shotSize.width
            self.screenShotWidthConstraint.constant = shotWidth
            self.screenShotCenterXConstraint.constant = shotSize.width > shotSize.height ? shotWidth : 0.0
            self.layoutIfNeeded()
        }
        
        if appData.screenShotUrls.count >= 3 {
            ImageOperationQueue.shared.setImage(with: self.screenShotImageViewFirst,
                                                defaultImage: model.cellScreentShotDefaultImage,
                                                url: appData.screenShotUrls[0],
                                                indexPath: indexPath,
                                                specailKey: model.firstShotKey)
            ImageOperationQueue.shared.setImage(with: self.screenShotImageViewSeconds,
                                                defaultImage: model.cellScreentShotDefaultImage,
                                                url: appData.screenShotUrls[1],
                                                indexPath: indexPath,
                                                specailKey: model.secondsShotKey)
            ImageOperationQueue.shared.setImage(with: self.screenShotImageViewThirds,
                                                defaultImage: model.cellScreentShotDefaultImage,
                                                url: appData.screenShotUrls[2],
                                                indexPath: indexPath,
                                                specailKey: model.thirdsShotKey)
        }
        else {
            self.screenShotImageViewFirst.image = model.cellScreentShotDefaultImage
            self.screenShotImageViewSeconds.image = model.cellScreentShotDefaultImage
            self.screenShotImageViewThirds.image = model.cellScreentShotDefaultImage
        }
    }
}
