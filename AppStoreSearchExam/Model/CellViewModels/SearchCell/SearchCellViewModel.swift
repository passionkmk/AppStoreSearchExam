//
//  SearchCellViewModel.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 14..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit

// MARK: - Init
class SearchCellViewModel: CellPresentation {
    var content: AppData
    
    let thumbnailKey: String = "SearchThumbnail"
    let firstShotKey: String = "SearchFirstShot"
    let secondsShotKey: String = "SearchSecondsShot"
    let thirdsShotKey: String = "SearchThirdsShot"
    
    init(content: AppData) {
        self.content = content
    }
}

// MARK: - Essential Function
extension SearchCellViewModel {
    func loadTableCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SearchCell", for: indexPath) as! SearchTableViewCell
        DispatchQueue.main.async {
            cell.setUI(self, indexPath: indexPath)
        }
        return cell
    }
}
