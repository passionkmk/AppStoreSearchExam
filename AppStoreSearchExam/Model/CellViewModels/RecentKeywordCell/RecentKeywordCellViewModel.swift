//
//  RecentKeywordCellViewModel.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 13..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit

// MARK: - Init
class RecentKeywordCellViewModel: CellPresentation {
    var keyword: String
    init(keyword: String) {
        self.keyword = keyword
    }
}

// MARK: - Essential Function
extension RecentKeywordCellViewModel {
    func loadTableCell(_ tableView: UITableView, indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RecentKeywordCell", for: indexPath) as! RecentKeywordTableViewCell
        cell.setLabel(self.keyword)
        return cell
    }
}
