//
//  RecentKeywordTableViewCell.swift
//  AppStoreSearchExam
//
//  Created by KimMinKu on 2018. 7. 13..
//  Copyright © 2018년 KimMinKu. All rights reserved.
//

import UIKit

// MARK: - Outlet
class RecentKeywordTableViewCell: UITableViewCell {
    @IBOutlet weak var keywordLabel: UILabel!
    let recentWordColor = UIColor(red: 0, green: 0.4784, blue: 1, alpha: 1.0)
}

// MARK: - Function
extension RecentKeywordTableViewCell {
    func setLabel(_ keyword: String) {
        self.keywordLabel.textColor = self.recentWordColor
        self.keywordLabel.text = keyword
    }
}
